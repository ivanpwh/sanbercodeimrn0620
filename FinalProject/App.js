// import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  Image,
  View,
  TextInput,
  StatusBar
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register'
import Profile from './components/Profile'

const initialState = {
  loginStatus: false,
  userLogin: {},
  bankData: [
    {
      username: 'admin',
      password: 'admin',
      name: 'Jhon Doe',
      job: 'Developer',
      birthDate: '17 August 1945'
    }
  ],
  username: '',
  password: '',
  name: '',
  job: '',
  birthDate: ''

}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_LOGIN_STATUS':
      state.loginStatus = !state.loginStatus
      return state
    case 'LOGIN':
      const filter = state.bankData.filter(item => { return (item.username === state.username.toLocaleLowerCase() && item.password === state.password) })
      if (filter.length > 0) {
        state.loginStatus = true
        state.userLogin = filter[0]
      } else {
        state.loginStatus = false
      }
      return state
    case 'REGISTER':
      state.bankData.push({
        username: state.username,
        password: state.password,
        name: state.name,
        job: state.job,
        birthDate: state.birthDate,
      })
      return state
    case 'SET_USERNAME':
      state.username = action.data.toLocaleLowerCase()
      return state
    case 'SET_PASSWORD':
      state.password = action.data
      return state
    case 'SET_NAME':
      state.name = action.data
      return state
    case 'SET_JOB':
      state.job = action.data
      return state
    case 'SET_BIRTH_DATE':
      state.birthDate = action.data
      return state

    default:
      return state
  }
}

const store = createStore(reducer)

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const HomeStackScreen = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerTitle: props => <Headerbar {...props} /> }}
      />
    </Stack.Navigator>
  )
}

const ProfileStackScreen = () => {
  return (
    <Provider store={store}>
      <Stack.Navigator>
        <Stack.Screen
          name="Profile"
          component={Profile}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Login"
          component={LoginStackScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </Provider>
  )
}

const LoginStackScreen = () => {
  return (
    <Provider store={store}>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </Provider>
  )
}

const TabStackScreen = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
      />
    </Tab.Navigator>
  )
}

const Headerbar = () => {
  return (
    <View style={styles.headerContainer}>
      <Image
        style={styles.headerLogo}
        source={{ uri: 'https://images.pexels.com/lib/api/pexels.png' }}
      />
      <TextInput
        style={styles.headerInput}
      />
      <Icon name='search' size={25} style={styles.headerIcon} />
    </View>
  )
}

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        barStyle='dark-content'
      />
      <NavigationContainer>
        <Drawer.Navigator>
          <Drawer.Screen
            name='Home'
            component={TabStackScreen}
          />
          <Drawer.Screen
            name='Profile'
            component={ProfileStackScreen}
          />
        </Drawer.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 5
  },
  headerIcon: {
    position: 'absolute',
    right: 10,
    zIndex: 10,
    color: 'grey'
  },
  headerInput: {
    height: 30,
    borderColor: 'gray',
    borderWidth: 1,
    width: 150,
    borderRadius: 20,
    paddingHorizontal: 10,
  },
  headerLogo: {
    width: 100,
    height: 50,
    resizeMode: 'contain'
  }
});
