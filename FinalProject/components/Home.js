import React, { Component } from 'react'
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  Linking,
  TouchableHighlight,
  Modal,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false,
      modalData: '',
      modalVisible: false,
      isLoadingModal: true,
      isErrorModal: false
    };
  }

  init = async () => {
    try {
      let response = await fetch('https://api.pexels.com/v1/search?query=people', {
        method: 'GET',
        headers: {
          Authorization: '563492ad6f917000010000016d1323cbd017442c82c2dbc39a9d2209'
        }
      })
      let json = await response.json();
      this.setState({ isError: false, isLoading: false, data: json })
    } catch (err) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  getPhoto = async (id) => {
    this.setState({ isLoadingModal: true, isErrorModal: false })
    try {
      let response = await fetch(`https://api.pexels.com/v1/photos/${id}`, {
        method: 'GET',
        headers: {
          Authorization: '563492ad6f917000010000016d1323cbd017442c82c2dbc39a9d2209'
        }
      })
      let json = await response.json();
      this.setState({ isErrorModal: false, isLoadingModal: false, modalData: json.src.large })
    } catch (err) {
      this.setState({ isLoadingModal: false, isErrorModal: true })
    }
  }

  componentDidMount() {
    this.init()
  }

  render() {
    const data = this.state.data.photos
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}>
          <View style={styles.container}>
            {!this.state.isLoadingModal
              ?
              <View style={styles.modalView}>
                <TouchableOpacity
                  style={{ position: "absolute", zIndex: 10, top: 10, right: 10 }}
                  onPress={() => { this.setState({ modalVisible: false, modalData: '' }) }}
                >
                  <Icon name="close-circle-outline" size={35} color={'grey'} />
                </TouchableOpacity>
                <Image
                  source={{ uri: this.state.modalData }}
                  style={{ width: 300, height: 450, borderRadius: 5 }}
                />
              </View>
              :
              <ActivityIndicator style={styles.loading} />
            }
          </View>
        </Modal>
        <FlatList
          data={data}
          keyExtractor={(item => item.id.toString())}
          renderItem={({ item }) =>
            <View style={styles.container}>
              <TouchableHighlight
                style={{ borderRadius: 10 }}
                onPress={() => { this.setState({ modalVisible: true }), this.getPhoto(item.id) }}>
                <Image
                  source={{ uri: item.src.portrait }}
                  style={styles.homeImage}
                />
              </TouchableHighlight>
              <View style={styles.homeTextContainer}>
                <Text style={{ fontSize: 16, color: '#1B2631' }}>Photo by </Text>
                <Text
                  style={{ fontSize: 16, color: '#5D6D7E', textDecorationLine: 'underline' }}
                  onPress={() => Linking.openURL(item.photographer_url)}>
                  {item.photographer}
                </Text>
                <Text style={{ fontSize: 12, color: '#1B2631' }}> on </Text>
                <Text
                  style={{ fontSize: 12, color: '#5D6D7E' }}
                  onPress={() => Linking.openURL('https://www.pexels.com/')}>
                  Pexel
                  </Text>
              </View>
            </View>
          }
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 5,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  homeImage: {
    width: 300,
    height: 300,
    borderRadius: 10
  },
  homeTextContainer: {
    marginVertical: 20,
    flexDirection: 'row',
    alignItems: 'flex-end'
  },
  loading: {
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 10
  }
});