import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import { connect } from 'react-redux'

class Profile extends Component {
  render() {
    return (
      <View style={styles.container}>
        {
          this.props.loginStatus === true
            ?
            <View style={styles.container}>
              <Icon name='profile' size={100} color={'grey'} />
              <Text style={styles.title}>User Profile </Text>
              <View style={styles.detailContainer}>
                <Text style={styles.detailText}>{this.props.name}</Text>
                <Text style={styles.detailText}>{this.props.job}</Text>
                <Text style={styles.detailText}>{this.props.birthDate}</Text>
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this.props.changeLoginStatus(), this.props.navigation.navigate('Profile')}>
                <Text style={styles.buttonText}>Logout</Text>
              </TouchableOpacity>
            </View>
            :
            <View style={styles.container}>
              <Text>You are not logged in.</Text>
              <Text style={{ marginBottom: 20 }}>Please log in to Access Profile </Text>
              <TouchableOpacity
                style={styles.button}
                onPress={() => { this.props.navigation.navigate('Login') }}>
                <Text style={styles.buttonText}>Go to Login</Text>
              </TouchableOpacity>
            </View>
        }
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    loginStatus: state.loginStatus,
    name: state.userLogin.name,
    job: state.userLogin.job,
    birthDate: state.userLogin.birthDate,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeLoginStatus: () => dispatch({ type: 'CHANGE_LOGIN_STATUS' })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  detailContainer: {
    backgroundColor: '#D6DBDF',
    width: 300,
    borderRadius: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 20
  },
  detailText: {
    color: '#1B2631',
    fontSize: 20,
    fontWeight: 'bold'
  },
  title: {
    fontSize: 36,
    fontWeight: 'bold',
    color: '#1B2631',
  },
  button: {
    width: 200,
    height: 30,
    backgroundColor: '#34495E',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  buttonText: {
    color: '#F3F3F3',
    fontSize: 16,
    fontWeight: 'bold'
  },
})

