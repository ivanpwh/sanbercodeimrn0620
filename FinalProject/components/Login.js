import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux'

class Login extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={{ uri: 'https://images.pexels.com/lib/api/pexels.png' }}
        />
        <View style={styles.inputContainer}>
          <Icon name='mail-outline' size={25} style={{ paddingRight: 10, color: '#333333' }} />
          <TextInput
            style={styles.inputText}
            placeholder='Username'
            keyboardType='email-address'
            onChangeText={(val) => this.props.setUserName(val)} />
        </View>
        <View style={styles.inputContainer}>
          <Icon name='lock-outline' size={25} style={{ paddingRight: 10, color: '#333333' }} />
          <TextInput
            style={styles.inputText}
            placeholder='Password'
            secureTextEntry={true}
            onChangeText={(val) => this.props.setPassword(val)} />
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => { this.props.login(), this.props.navigation.push('Profile') }}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
        <View style={styles.textBottomContainer}>
          <Text style={styles.textBottom}>Don't have an account?
          </Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
            <Text style={styles.textBottomBold}> Create One</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    loginStatus: state.loginStatus,
    username: state.username,
    password: state.password
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeLoginStatus: () => dispatch({ type: 'CHANGE_LOGIN_STATUS' }),
    setUserName: (item) => dispatch({ type: 'SET_USERNAME', data: item }),
    setPassword: (item) => dispatch({ type: 'SET_PASSWORD', data: item }),
    login: () => dispatch({ type: 'LOGIN' })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 200,
    height: 72,
    resizeMode: 'contain',
    marginBottom: 40
  },
  inputText: {
    width: '100%',
    maxWidth: 150
  },
  inputContainer: {
    width: 200,
    height: 30,
    backgroundColor: '#D6DBDF',
    borderRadius: 5,
    alignItems: "center",
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 40
  },
  button: {
    width: 200,
    height: 30,
    backgroundColor: '#5D6D7E',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  buttonText: {
    color: '#F3F3F3',
    fontSize: 16,
    fontWeight: 'bold'
  },
  textBottom: {
    color: '#1B2631'
  },
  textBottomBold: {
    fontWeight: 'bold',
    color: '#1B2631'
  },
  textBottomContainer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 10,
  }
})

