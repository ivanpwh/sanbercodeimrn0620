import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import { connect } from 'react-redux'

class Register extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.logo}
          source={{ uri: 'https://images.pexels.com/lib/api/pexels.png' }}
        />
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputText}
            placeholder='Username'
            keyboardType='email-address'
            onChangeText={(val) => this.props.setUserName(val)} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputText}
            placeholder='Password'
            secureTextEntry={true}
            onChangeText={(val) => this.props.setPassword(val)} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputText}
            placeholder='Your Name'
            onChangeText={(val) => this.props.setName(val)} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputText}
            placeholder='Your Job'
            onChangeText={(val) => this.props.setJob(val)} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.inputText}
            placeholder='Your Birth Date'
            onChangeText={(val) => this.props.setBirthDate(val)} />
        </View>
        <TouchableOpacity
          style={styles.button}
          onPress={() => { this.props.register(), this.props.navigation.push('Login') }}>
          <Text style={styles.buttonText}>REGISTER</Text>
        </TouchableOpacity>
        <View style={styles.textBottomContainer}>
          <Text style={styles.textBottom}>Already have account?
          </Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.textBottomBold}> Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    loginStatus: state.loginStatus
  }
}

function mapDispatchToProps(dispatch) {
  return {
    setUserName: (item) => dispatch({ type: 'SET_USERNAME', data: item }),
    setPassword: (item) => dispatch({ type: 'SET_PASSWORD', data: item }),
    setName: (item) => dispatch({ type: 'SET_NAME', data: item }),
    setJob: (item) => dispatch({ type: 'SET_JOB', data: item }),
    setBirthDate: (item) => dispatch({ type: 'SET_BIRTH_DATE', data: item }),
    register: () => dispatch({ type: 'REGISTER' }),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: 200,
    height: 72,
    resizeMode: 'contain',
    marginBottom: 40
  },
  inputText: {
    width: '100%',
    maxWidth: 150
  },
  inputContainer: {
    width: 200,
    height: 30,
    backgroundColor: '#D6DBDF',
    borderRadius: 5,
    alignItems: "center",
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 10
  },
  button: {
    width: 200,
    height: 30,
    backgroundColor: '#5D6D7E',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
  },
  buttonText: {
    color: '#F3F3F3',
    fontSize: 16,
    fontWeight: 'bold'
  },
  textBottom: {
    color: '#1B2631'
  },
  textBottomBold: {
    fontWeight: 'bold',
    color: '#1B2631'
  },
  textBottomContainer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 10,
  }
})

