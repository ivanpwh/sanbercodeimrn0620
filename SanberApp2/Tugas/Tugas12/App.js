import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native'
import Navbar from './components/Navbar'
import Tabbar from './components/Tabbar'
import Content from './components/Content'



export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Navbar />
        <Content />
        <Tabbar />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})