import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'


class Navbar extends Component {
  render() {
    return (
      <View style={styles.nav}>
        <Image source={require('../images/logo.png')} style={{width: 98, height: 22}}></Image>
        <View style={styles.navRight}>
          <TouchableOpacity>
            <Icon name="search" size={25} style={styles.navItem}/>
          </TouchableOpacity>
          <TouchableOpacity>
            <Icon name="account-circle" size={25} style={styles.navItem}/>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  nav: {
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    height: 50,
    flexDirection: 'row',
    alignItems: "center",
    borderBottomWidth: 0.5,
    borderBottomColor: 'grey',
    justifyContent: "space-between"
  },
  navRight: {
    flexDirection: "row",
  },
  navItem: {
    color: 'grey',
    marginLeft: 25,
  }
})

 
export default Navbar;