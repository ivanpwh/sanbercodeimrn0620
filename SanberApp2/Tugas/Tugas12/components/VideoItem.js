import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

class VideoItem extends Component {
  render() { 
    let video = this.props.video
    return (
      <View style={styles.container}>
        <Image source={{uri:video.snippet.thumbnails.medium.url}} style={{height: 200}}/>
        <View style={styles.descContainer}>
          <Image source={{ uri:'https://randomuser.me/api/portraits/med/men/14.jpg' }} style={{width: 50, height: 50, borderRadius: 25}}/>
          <View style={styles.videoDetails}>
            <Text style={styles.videoTitle}>{video.snippet.title}</Text>
            <Text style={styles.videoStats}>{video.snippet.channelTitle + " . " + video.statistics.viewCount + " views . 3 months ago"}</Text>
          </View>
          <TouchableOpacity>
            <Icon name="more-vert" size={20} color="grey"/>
          </TouchableOpacity>
        </ View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 15,
    paddingBottom: 15,
    borderBottomWidth: 0.2,
    borderBottomColor: 'grey',
  },
  descContainer: {
    flexDirection: "row",
    paddingTop: 15,
  },
  videoTitle: {
    fontSize: 14,
    color: 'grey'
  },
  videoDetails: {
    paddingHorizontal: 15,
    flex: 1
  },
  videoStats: {
    color: '#3c3c3c',
    fontSize: 12,
    paddingTop: 3
  }
})

 
export default VideoItem;