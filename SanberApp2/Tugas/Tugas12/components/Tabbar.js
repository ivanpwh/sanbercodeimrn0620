import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

class Tabbar extends Component {
  render() { 
    return (
      <View style={styles.tab}>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="home" size={25} style={styles.tabIcon}/>
          <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="whatshot" size={25} style={styles.tabIcon}/>
          <Text style={styles.tabTitle}>Trending</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="subscriptions" size={25} style={styles.tabIcon}/>
          <Text style={styles.tabTitle}>Subscriptions</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
          <Icon name="folder" size={25} style={styles.tabIcon}/>
          <Text style={styles.tabTitle}>Library</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tab: {
    backgroundColor: 'white',
    height: 50,
    borderTopWidth: 0.5,
    borderTopColor: '#E5E5E5',
    flexDirection: "row",
    justifyContent: "space-around",
  },
  tabItem: {
    alignItems: "center",
    justifyContent: "center",
  },
  tabIcon: {
    color: "grey",
  },
  tabTitle: {
    fontSize: 11,
    paddingTop: 3,
    color: "grey",
  }
})

 
export default Tabbar;