import React, { Component } from 'react';
import { View, StyleSheet, FlatList } from 'react-native'
import data from '../data.json'
import VideoItem from './VideoItem'

class Content extends Component {
  render() {
    return (
      <View style={styles.component}>
        <FlatList 
          data={data.items}
          renderItem={(video) => <VideoItem video={video.item}/>}
          keyExtractor={(item=>item.id)}
          ItemSeparatorComponent={()=><View style={{height: 0.5, backgroundColor: '#cccccc'}} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  component: {
    flex: 1,
    padding: 15
  },
  
})

export default Content;