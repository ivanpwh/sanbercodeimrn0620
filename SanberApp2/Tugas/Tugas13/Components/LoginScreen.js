import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, TextInput, TouchableOpacity } from 'react-native'

class LoginScreen extends Component {
  render() { 
    return (
      <View style={styles.container}>
        <Image source={require('../Images/logo.png')} style={{width: '100vw', height: 100}}></Image>
        <Text style={styles.title}>Login</Text>
        <View>
          <Text style={styles.inputText}>Username / Email</Text>
          <TextInput style={styles.inputBox} />
          <Text style={styles.inputText}>Password</Text>
          <TextInput style={styles.inputBox} />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity>
            <View style={styles.buttonMasuk}>
              <Text style={styles.buttonText}>Masuk</Text>
            </View>
          </TouchableOpacity>
          <Text style={styles.textAtau}>Atau</Text>
          <TouchableOpacity>
            <View style={styles.buttonDaftar}>
              <Text style={styles.buttonText}>Daftar ?</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  title: {
    fontSize: 24,
    marginTop: 70,
    marginBottom: 40,
    color: '#003366'
  },
  inputText: {
    fontSize: 16,
    color: '#003366'
  },
  inputBox: {
    height: 48,
    width: 294,
    borderColor: 'gray', 
    borderWidth: 1,
    marginBottom: 16
  },
  textAtau: {
    marginVertical: 16,
    fontSize: 24,
    color: '#3EC6FF'
  },
  buttonMasuk: {
    backgroundColor: '#3EC6FF',
    width: 140,
    height: 40,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: "center"
  },
  buttonDaftar: {
    backgroundColor: '#003366',
    width: 140,
    height: 40,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 24
  },
  buttonContainer: {
    justifyContent:"center",
    alignItems: "center",
    marginVertical: 10
  }
})

 
export default LoginScreen;