import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

class AboutScreen extends Component {
  state = {  }
  render() { 
    return (
      <View>
        <Text>Tentang Saya</Text>
        <Icon name="account-circle" size={25}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    alignItems: "center"
  }
})

 
export default AboutScreen;