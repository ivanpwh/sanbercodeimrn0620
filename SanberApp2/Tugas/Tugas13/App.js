import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native'
import LoginScreen from './Components/LoginScreen'
import RegisterScreen from './Components/RegisterScreen'
import AboutScreen from './Components/AboutScreen'

class App extends Component {
  render() { 
    return (
      <View style={styles.container}>
        <LoginScreen />
        <RegisterScreen />
        <AboutScreen />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    paddingTop: 68
  },
})


export default App;