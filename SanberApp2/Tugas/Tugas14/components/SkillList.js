import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import Icon2 from 'react-native-vector-icons/SimpleLineIcons'
import Logo from 'react-native-vector-icons/MaterialCommunityIcons'

export default class SkillList extends Component {
  render() {
    let item = this.props.item
    return (
      <View style={styles.listContainer}>
        <Logo name={item.logoUrl} size={100} style={{color: '#003366'}} />
        <View style={styles.listDetail}>
          <Text style={styles.listTitle}>{item.skillName}</Text>
          <Text style={styles.listCategory}>{item.categoryName}</Text>
          <View style={styles.listPrecentContainer}>
            <Text style={styles.listPercent}>{item.percentageProgress}</Text>
          </View>
        </View>
        <Icon2 name='arrow-right' size={50} style={{color: '#003366'}}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  listContainer: {
    backgroundColor: '#B4E9FF',
    borderRadius: 8,
    alignItems: "center",
    padding: 10,
    flexDirection: "row",
    marginBottom: 10
  },
  listTitle: {
    fontSize: 24,
    fontWeight: "bold",
    color: '#003366'
  },
  listCategory: {
    fontSize: 16,
    fontWeight: "bold",
    color: '#3EC6FF'
  },
  listPercent: {
    fontSize: 48,
    fontWeight: "bold",
    color: 'white',
    position: "absolute",
    right: 0,
  },
  listPrecentContainer: {
    position: "relative",
    height: 64
  },
  listDetail: {
    marginHorizontal: 10,
    width: '50%'
  }
})

