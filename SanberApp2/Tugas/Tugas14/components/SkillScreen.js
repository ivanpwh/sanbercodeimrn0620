import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import data from '../skillData.json'
import SkillList from './SkillList'

export default class SkillScreen extends Component {
  render() {
    let item = data.items
    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={require('../../Tugas13/Images/logo.png')} style={styles.logo}/>
        </View>
        <View style={styles.account}>
          <Icon style={styles.accountIcon} name="user-circle" size={40}/>
          <View style={styles.accountDetail}>
            <Text style={styles.accountText1}>Hai,</Text>
            <Text style={styles.accountText2}>Ivan Putra Widyatama Hukom</Text>
          </View>
        </View>
        <Text style={styles.skill}>SKILL</Text>
        <View style={styles.skillDetail}>
          <View style={styles.skillContainer}>
            <Text style={styles.skillText}>Library / Framework</Text>
          </View>
          <View style={styles.skillContainer}>
            <Text style={styles.skillText}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.skillContainer}>
            <Text style={styles.skillText}>Teknologi</Text>
          </View>
        </View>
        <FlatList
          data = {data.items}
          keyExtractor= {(item=>item.id)}
          renderItem={(items) => <SkillList item={items.item}/>}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10
  },
  logoContainer: {
    position: "relative",
    width: '100%',
    height: 50
  },
  logo: {
    width: 200,
    height: 50,
    resizeMode: 'contain',
    position: "absolute",
    right: 0
  },
  account: {
    flexDirection: "row",
  },
  accountDetail: {
    flexDirection: "column",
    paddingLeft: 10
  },
  accountIcon: {
    color: '#3EC6FF'
  },
  accountText1: {
    fontSize: 12
  },
  accountText2: {
    fontSize: 16,
    color: '#003366'
  },
  skill: {
   fontSize: 36,
   color: '#003366',
   width: "100%",
   borderBottomWidth: 4,
   borderBottomColor: '#3EC6FF'
  },
  skillContainer: {
    padding: 8,
    borderRadius: 8,
    backgroundColor: '#B4E9FF',
    marginHorizontal: 3
  },
  skillText: {
    color: '#003366',
    fontSize: 12,
    fontWeight: "bold"
  },
  skillDetail: {
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: 'space-between',
  }
})

