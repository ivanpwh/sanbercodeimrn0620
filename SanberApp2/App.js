import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Tugas12 from './Tugas/Tugas12/App'
import Tugas13 from './Tugas/Tugas13/App'
import Tugas14 from './Tugas/Tugas14/App'
import Tugas15 from './Tugas/Tugas15/index'

export default function App() {
  return (
    <View style={styles.container}>
      {/* <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" /> */}
      {/* <Tugas12 /> */}
      {/* <Tugas13 /> */}
      <Tugas14 />
      {/* <Tugas15 /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});