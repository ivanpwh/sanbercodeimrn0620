var i = 0

console.log('LOOPING PERTAMA');
while (i < 20) {
  i += 2;
  console.log(i.toString(), '- I love coding');  
}
console.log('LOOPING KEDUA');
while (i > 0) {
  console.log(i.toString(), '- I will become a mobile developer');
  i -= 2;
}

for(var i = 1 ; i <= 20; i++) {
  if (i % 3 === 0) {
    console.log(i.toString(), '- I Love Coding');
  } else if (i % 2 === 0) {
    console.log(i.toString(), '- Berkualitas');
  } else if (i % 2 === 1) {
    console.log(i.toString(), '- Santai');
  }
}

var j = 1
while (j <= 4) {
  j += 1
  for(var i = 1; i <= 8; i++) {
    process.stdout.write('#')
  }
  console.log();
}

var k = 0
while (k <= 7) {
  k += 1
  for (var i = 1; i <= k; i++) {
    // console.log('i', i)
    process.stdout.write('#')
  }
  console.log();
  
}

var l = 1
while (l <= 8) {
  l += 1
  if (l % 2 === 0) {
    for (var i = 1; i <= 8; i++){
      if (i % 2 === 0) {
        process.stdout.write(' ')
      } else if (i % 2 === 1) {
        process.stdout.write('#')
      }
    }
    console.log();
  } else if (l % 2 === 1) {
    for (var i = 1; i <= 8; i++){
      if (i % 2 === 0) {
        process.stdout.write('#')
      } else if (i % 2 === 1) {
        process.stdout.write(' ')
      }
    }
    console.log();
  }
}