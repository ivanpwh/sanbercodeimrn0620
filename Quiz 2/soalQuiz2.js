/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  // Code disini
  constructor({subject, points, email}) {
    this.subject = subject
    this.points = points
    this.email = email
    this.total = 0
  }

  average (data) {
    for (var i in data) {
      this.total += data[i]
    }
    console.log(`Rata - rata: ${this.total}`);
  }

  show() {
    if (this.points.length > 1) {
      console.log(`Subject: ${this.subject}`);
      this.average(this.points)
      console.log(`Email: ${this.email}`);
    } else {
      console.log(`Subject: ${this.subject}\nPoint: ${this.points}\nEmail: ${this.email}`);
    }
  }
}

var dataSoal1 = {
  subject: 'Ayu',
  points: [10, 20, 30, 40, 50],
  email: 'ayu@gmail.com'
} 

var soal1 = new Score (dataSoal1)
soal1.show()

console.log();

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
  // code kamu di sini
  var result = []
  var dataIndex = data[0].indexOf(subject)

  for (let i = 1; i < data.length; i++) {
    result.push({
      email: data[i][0],
      subject: data[i][dataIndex]
    })
  }

  for (var i in result) {
    console.log(result[i]);
  }

  console.log();
}

// TEST CASE
viewScores(data, "quiz - 1")
viewScores(data, "quiz - 2")
viewScores(data, "quiz - 3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data) {
  // code kamu di sini
  var result = []
  let [detail, ...restOfData] = data

  for (var i in restOfData) {
    var total = 0
    var predikat = ''
    let [email, ...points] = restOfData[i]
    
    for (var j in points) {
      total += points[j] 
    }

    var average = (total / points.length).toFixed(1)
    
    if (average > 90) {
      predikat = 'honour'
    } else if (average > 80) {
      predikat = 'graduate'
    } else {
      predikat = 'participant'
    }

    result.push({
      email,
      average,
      predikat
    })
  }
  
  for (var k in result) {
    console.log(`${parseInt(k) + 1}. Email: ${result[k].email}\nRata - rata: ${result[k].average}\nPredikat: ${result[k].predikat}\n`);
  }
}

recapScores(data);