function range(startNum, finishNum) {
  var temp = []
  if (startNum === undefined || finishNum === undefined) {
    temp = -1
  } else if (startNum > finishNum) {
    for (startNum; startNum >= finishNum; startNum--) {
      temp.push(startNum)
    }
  } else if (startNum < finishNum) {
    for (startNum; startNum <= finishNum; startNum++) {
      temp.push(startNum)
    }
  }
  return (temp)
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

function rangeWithStep(startNum, finishNum, step) {
  var temp = []
  if (startNum === undefined || finishNum === undefined) {
    temp = -1
  } else if (startNum > finishNum) {
    for (startNum; startNum >= finishNum; startNum -= step) {
      temp.push(startNum)
    }
  } else if (startNum < finishNum) {
    for (startNum; startNum <= finishNum; startNum += step) {
      temp.push(startNum)
    }
  }
  return (temp)
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

function sum(startNum, finishNum, step) {
  var temp = 0
  if (startNum === undefined) {
    temp = 0
  } else if (finishNum === undefined) {
    temp = startNum
  } else if (step === undefined && startNum > finishNum) {
    for (startNum; startNum >= finishNum; startNum--) {
      temp += startNum
    }
  } else if (step === undefined && startNum < finishNum) {
    for (startNum; startNum <= finishNum; startNum++) {
      temp += startNum
    }
  } else if (startNum > finishNum) {
    for (startNum; startNum >= finishNum; startNum -= step) {
      temp += startNum
    }
  } else if (startNum < finishNum) {
    for (startNum; startNum <= finishNum; startNum += step) {
      temp += startNum
    }
  }
  return (temp)
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

function dataHandling(data) {
  for (var i = 0; i <= data.length - 1; i++) {
    console.log('Nomor ID:', data[i][0])
    console.log('Nama Lengkap:', data[i][1])
    console.log('TTL:', data[i][2], data[i][3])
    console.log('Hobi:', data[i][4])
    console.log();
  }
}

//contoh input
var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

function balikKata(data) {
  
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
// console.log(balikKata("SanberCode")) // edoCrebnaS
// console.log(balikKata("Haji Ijah")) // hajI ijaH
// console.log(balikKata("racecar")) // racecar
// console.log(balikKata("I am Sanbers")) // srebnaS ma I 

function dataHandling2(data) {
  var bulan = data[3].split('/')
  var nama = data[1].split(' ')
  
  switch (bulan[1]) {
    case '01':
      bulan = 'Januari'
      break;
    case '02':
      bulan = 'Februari'
      break;
    case '03':
      bulan = 'Maret'
      break;
    case '04':
      bulan = 'April'
      break;
    case '05':
      bulan = 'Mei'
      break;
    case '06':
      bulan = 'Juni'
      break;
    case '07':
      bulan = 'Juli'
      break;
    case '08':
      bulan = 'Agustus'
      break;
    case '09':
      bulan = 'September'
      break;
    case '10':
      bulan = 'Oktober'
      break;
    case '11':
      bulan = 'November'
      break;
    case '12':
      bulan = 'Desember'
      break;
  }

  console.log(data)
  console.log(bulan);
  console.log(data[3].split('/').join('-'));
  console.log(nama[0], nama[1])
}


var input = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"];
dataHandling2(input);
