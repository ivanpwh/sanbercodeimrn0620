var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(params) {
  var object = []

  for (item in params) {
    object.push({
      id: parseInt(item) + 1,
      firstname: params[item][0],
      lastName: params[item][1],
      gender: params[item][2],
      age: params[item][3] < thisYear ? thisYear - parseInt(params[item][3]) : 'Invalid Birth Year'
    })
  }

  for (item in object) {
    console.log(object[item].id + '. ' + object[item].firstname, object[item].lastName + ' { firstName: ' + object[item].firstname + ', lastName: ' + object[item].lastName + ', gender: ' + object[item].gender + ', age: ' + object[item].age + ' }');
  }
  
}

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

// Error case 
arrayToObject([]) // ""

console.log();

function shoppingTime(memberId, money) {
  // you can only write your code here!
  var result = []
  var sale = [
    { item: 'Sepatu Stacattu', harga: 1500000 },
    { item: 'Baju Zoro', harga: 500000 },
    { item: 'Baju H&N', harga: 250000 },
    { item: 'Sweater Uniklooh', harga: 175000 },
    { item: 'Casing Handphone', harga: 50000 },
  ]
  var lastMoney = money
  var listPurchased = []

  if (memberId === undefined || memberId === '') {
    result.push('Mohon maaf, toko X hanya berlaku untuk member saja')
  } else if (money < 50000 ) {
   result.push('Mohon maaf, uang tidak cukup') 
  } else {
    for (i in sale) {
      if (lastMoney > sale[i].harga) {
        lastMoney -= sale[i].harga
        listPurchased.push(sale[i].item)
      }
    }
    result.push({
      memberId: memberId,
      money: money,
      listPurchased: listPurchased,
      changeMoney: lastMoney
    })
  }
  return result[0]
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log();

function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var hasil = []

  //your code here
  for (i in arrPenumpang) {
    hasil.push({
      penumpang: arrPenumpang[i][0],
      naikDari: arrPenumpang[i][1],
      tujuan: arrPenumpang[i][2]
    })
  }

  for (i in hasil) {
    var bayar = 0

    for (let index = check(hasil[i].naikDari); index < check(hasil[i].tujuan); index++) {
      bayar += 2000
    }

    var total = { bayar: bayar}
    Object.assign(hasil[i], total)
  }

  return hasil
}

function check(item) {
  var temp = 0

  if (item === 'A') {
    temp = 0
  }
  if (item === 'B') {
    temp = 1
  }
  if (item === 'C') {
    temp = 2
  }
  if (item === 'D') {
    temp = 3
  }
  if (item === 'E') {
    temp = 4
  }
  if (item === 'F') {
    temp = 5
  }

  return temp
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]


