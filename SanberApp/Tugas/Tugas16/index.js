import React, { Component } from 'react'
import { Text, View, StyleSheet, FlatList } from 'react-native'

export default class index extends Component {
  state = {
    data: []
  }

  componentDidMount() {
    this.fetchData()
  }

  fetchData = async () => {
    const response = await fetch('https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole')
    const json = await response.json()
    this.setState({ data: json })
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList data={this.state.data}
          keyExtractor={(x, i) => i}
          renderItem={({item}) => 
            <Text>
              {`${item.first} ${item.last}`}
            </Text>
          }
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

