var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i = 0
var time = 10000

readBooksPromise(time, books[i])
  .then((fulfilled) => {
    i += 1
    time = fulfilled
    readBooksPromise(time, books[i])
      .then((fulfilled) => {
        i += 1
        time = fulfilled
        readBooksPromise(time, books[i])
      })
      .catch((err) => {
        console.log(err);
      })
  }).catch((err) => {
    console.log(err);
  })