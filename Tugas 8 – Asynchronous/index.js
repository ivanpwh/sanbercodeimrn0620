// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0
var time = 10000
readBooks(time, books[i], (callback) => {
  if (callback) {
    i += 1
    time = callback
    readBooks(time, books[i], (callback) => {
      if (callback) {
        i += 1
        time = callback
        readBooks(time, books[i], () => {})
      }
    })
  }
})